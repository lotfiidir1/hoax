const request   = require('request');
var express     = require('express'),
fs              = require('fs'),

cheerio         = require('cheerio'),
app             = express();

var urls = 'http://www.politifact.com/truth-o-meter/statements/';
var docs = [];
var visited = [];

app.get('/scrape', function(req, res){
    console.log('----------------- APP LAUNCH ----------------');
    getPolitifacData(urls, function () {
        //delete old data
        fs.writeFile('../resources/data/output.json', '');

        //add new data
        fs.writeFile('../resources/data/output.json', JSON.stringify(docs, null, 4), function(err){
            console.log('File successfully written! - Check your project directory for the output.json file');
        })       
    })
})

/**
 * Extract all data in politifac
 * 
 * @param {*String : politifac url} url 
 * @param {*Callback} callback 
 */
function getPolitifacData(url, callback) {
    request.get({
        url: url,
    }, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            var $ = cheerio.load(body);
            $('.scoretable__item').filter(function(){
                var dom = $(this);
                docs.push(extractItemData(dom));
            })

            if(hasNext(body)){
                var next = getNextPageLink(body);            
                if( next != '' && ! existsIn(visited, next) ){
                    visited.push(next);
                    getPolitifacData(urls+next, callback);
                    //console.log('NEXT =====================> '+urls+next);
                }
            } else {
                callback();
                console.log(' --------------------- END OF SCRAPING --------------------');                
            }
        } else {
            console.log(error);
            throw error;
        }
    });
}

/**
 * Send true if element exist in tab
 * 
 * @param {*Array} tab 
 * @param {*String} elt 
 */
function existsIn(tab, elt){
    for(var i = 0; i < tab.length; i++){
        if( tab[i] == elt )
            return true;
    }
    return false;
}

/**
 * Send true if page has a next
 * 
 * @param {*HTML page} dom 
 */
function hasNext(dom){
    var resp = false;
    var $ = cheerio.load(dom);    
    $('.step-links').filter(function(){
        var dom = $(this);
        if(dom.find('.step-links__next') != ''){
            resp = true;
        }
    })
    return resp;
}

/**
 * Extract the next page
 * 
 * @param {*HTML page} dom 
 */
function getNextPageLink(dom){
    var resp = '';
    var $ = cheerio.load(dom);    
    $('.step-links').filter(function(){
        var dom = $(this);
        if(dom.find('.step-links__next') != ''){
            resp = dom.find('.step-links__next').attr('href');
        }
    })
    return resp;
}

/**
 * Extract document item in DOM
 *
 * @param {*HTML DOM} data
 */
function extractItemData(data){
    var json = { meter_link: "", meter_img: "", meter_type: "", meter_detail: "", body_authorLink: "", body_authorImg: "", body_authorName: "", body_detail: "", body_detailLink: "", title: "", body_link: "", body_editor: "", body_editorLink: "", body_date: Date, date_timestamp: "" };

    var meter = data.children().first().children().first();
    var statement_body = meter.next().children();
    var statement_body_first_child = statement_body.first();

    json.meter_link = meter.children('a').attr('href');
    json.meter_img = meter.children('a').children('img').attr('src');
    json.meter_type = meter.children('a').children('img').attr('alt');
    json.meter_detail = cleanner(meter.children('p').text());

    json.body_authorLink = statement_body_first_child.children('a').attr('href');
    json.body_authorImg = statement_body_first_child.children('a').children('img').attr('src');
    json.body_authorName = statement_body_first_child.children('a').children('img').attr('alt');

    json.body_detailLink = statement_body_first_child.next().children('a').attr('href');
    json.body_detail = cleanner(statement_body_first_child.next().children('a').text());

    json.title = cleanner(statement_body_first_child.next().next().children('a').text());
    json.body_link = statement_body_first_child.next().next().children('a').attr('href');

    json.body_editor = cleanner(statement_body_first_child.next().next().next().children('a').text());
    json.body_editorLink = statement_body_first_child.next().next().next().children('a').attr('href');
    date = statement_body_first_child.next().next().next().children('span').text();
    json.body_date = date.replace('on ', '');
    json.date_timestamp = Date.parse(treateDate(date.replace('on ', '')));;

    return json;
}

/**
 * Give date in format Month, day Year
 * 
 * @param {*String} strDate 
 */
function treateDate(strDate){
    var input = strDate.split(',');
    var month = input[1].replace(/[st]+$/gm, '').replace(/[th]+$/gm, '').replace(/[nd]+$/gm, '').replace(/[rd]+$/gm, '');

    return cleanner(month+input[2]);
}

/**
 * Extract tabulation,etc.
 * 
 * @param {*String} str 
 */
function cleanner( str ){
    var beginCleanner = /^[ |\n|"|—]+/gm;
    var endCleanner = /[\s|\n|"]+$/gm;

    var res  = str.replace(/"/, '');
    res = res.replace(/\n/, '');
    res = res.replace(/\t/, '');
    res = res.replace(beginCleanner, '');
    res = res.replace(endCleanner, '');

    return res;
}

/**
 * 
 */
function storageData(){
    fs.writeFile('output.json', JSON.stringify(docs, null, 4), function(err){
        console.log('File successfully written! - Check your project directory for the output.json file');
    })
}



app.listen('8081')
console.log('Magic happens on port 8081');
