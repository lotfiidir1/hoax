# Hoax search engine
par Siriman TRAORE - Lotfi IDIR
# Description du projet
Ce projet est fait en deux parties la partie serveur qui récupère les données du site [politifact.com](politifact.com) et partie IHM qui fait l'indexation et offre la possibilité de rechercher en text plain.

La partie serveur est dans le dossier **scraper/** 
# Requirement
- NodeJs

# Installation

- Partie serveur
- se mettre dans le dossier **scraper** puis lancer la commande : 

> `npm install 
> npm start`

Ouvrir le navigateur sur le [localhost:8081/scrape](localhost:8081/scrape)
A l'affichage du message "File successfully written ...." dans la console 
ça voudra dire que la récupération a été mise sur **resources/data/output.json"**

Vous pouvez commencer à tester l'application en ouvrant index.html dans le navigateur.