var index = elasticlunr(function () {
    this.addField('title');
    this.addField('body_authorName');
    this.addField('body_editor');
    //this.addField('body_date');
    //this.addField('meter_detail');
    //this.addField('meter_type');
    this.setRef('id');
});

var globalContent;
var allCheckbox = [];

/**
 * Load page
 */
$(document).ready(function () {
    initialization();
    $('.input-daterange input').each(function () {
        $(this).datepicker({
            clearDates : '',
            format: 'yyyy/mm/dd'
        });
    });
});

/**
 * Create all index with Elasticlunr
 */
function initialization() {
    $.getJSON("resources/data/output.json", function (json) {
        $.each(json, function (key, value) {
            var id = "id";
            value[id] = key;
            index.addDoc(value);
        });
    }).done(function () {
        $("#overlay-loader").hide();
    });
}

/**
 * Check action on search input
 */
$( "input#search" ).keyup(function (e) {
    var searchData = $( this ).val();
    console.log(searchData);

    globalContent = search(searchData);
    //globalContent = index.search(searchData);
    globalContent = updateTmp(globalContent);
    if (allCheckbox.length != 0) {
        updater(sortByType(allCheckbox, globalContent));
        return;
    }
    updater(globalContent);
    if(searchData == ''){
        $('#content .hoax-rows').empty();
    }
    return false;
});


/**
 * Search entry by elasticlunr indexation
 * system based on tf-idf method
 *
 * @param {*String} entry
 */
function search(entry){
    return index.search(entry, {
        fields: {
            title: {boost: 2},
            body_authorName:{boost: 1},
            body_editor: {boost: 1}
        }
    });
}

/**
 * Update page content
 *
 * @param {*Array of documents} params
 */
function updater(params){
    $('#content .hoax-rows').empty();
    $('#content h3').empty();

    template(params);
    $('#content .hoax-rows .hoax-row .block-text .p').text().substring('0', '50');
    updatePagination();
    if (params.length <= 0 ){
        $('.holder').empty();
    }

}

/**
 * Check gesture on checkbox
 */
$( 'input.meter-item' ).change(function (){
    var tmp = updateTmp(globalContent);
    var txt = $( this ).next().text();

    if( isChecked( $(this) ) ) {
        allCheckbox.push(txt);
        updater(sortByType(allCheckbox, tmp));
    } else {
        var pos = allCheckbox.indexOf(txt);
        allCheckbox.splice(pos, 1);
        if (allCheckbox.length == 0 ) {
            updater(tmp);
            return;
        }
        updater(sortByType(allCheckbox, tmp));
    }
});

/**
 * Do search by date
 */
$( "input.form-control" ).change(function() {
    var $begin = $('input[name="begin-date"]');
    var $end = $('input[name="end-date"]');
    if( $begin.val() === '' || $end.val() === '' )
      return;

    var beginDate = Date.parse($begin.val());
    var endDate = Date.parse($end.val());

    if(beginDate > endDate){
        alert('Begin date will not be > to End date');
        return;
    }

    //All verification is OK
    var tmp = globalContent;
    if (allCheckbox.length != 0) {
        tmp = sortByType(allCheckbox, tmp);
    }
    updater(sortByDate(beginDate, endDate,tmp));
});

/**
 * Give true if checkbox is checked else false
 * 
 * @param {*Checkbox} input 
 */
function isChecked(input){
    return input.is(':checked');
}

/**
 * Give true if dates's input aren't empty
 */
function hasDates(){
    var $begin = $('input[name="begin-date"]');
    var $end = $('input[name="end-date"]');
    return $begin.val() === '' || $end.val() === '';
}

function updateTmp(tab){
    var $begin = $('input[name="begin-date"]');
    var $end = $('input[name="end-date"]');
    if( $begin.val() === '' || $end.val() === '' )
      return tab;

    var beginDate = Date.parse($begin.val());
    var endDate = Date.parse($end.val());

    return sortByDate(beginDate, endDate,tab);
}

/**
 * Short array by meter-type
 *
 * @param {*metert type} type
 * @param {* Array} tab
 */
function sortByType(type, tab){
    var res = [];
    var i, j;
    for (var i = 0; i < type.length; i++) {
        for(j = 0; j < tab.length; j++){
            if(type[i] == tab[j].doc.meter_type){
                res.push(tab[j]);
            }
        }
    }
    return res;
}

/**
 * Sort array between begin date and end date
 * 
 * @param {*String} bdate 
 * @param {*String} edate 
 * @param {*Array} tab 
 */
function sortByDate(bdate, edate, tab){
    var res = [];
    for (var i = 0; i < tab.length; i++) {
        if( bdate <= tab[i].doc.date_timestamp && edate >= tab[i].doc.date_timestamp ){
            res.push(tab[i]);
        }
    }
    return res;
}

/**
 * Sort array by asceding date
 * 
 * @param {*Array} tab 
 */
function sorter(tab){
    tab.sort(function(a, b){
        return b.doc.date_timestamp - a.doc.date_timestamp;
    });
    return tab;
}


/**
 * Display search'resulat data
 * 
 * @param {*Array} data
 */
function template(data) {
    if ((data.length !== 0) && ($( "input#search" ).val())){
        for (var i = 0; i < data.length; i++) {
            // Reformat meter type for class
            var classMeter = data[i].doc.meter_type.toLowerCase().replace(" ", "-").replace(" ", "-").replace("!", "");

                $('#content .hoax-rows').append('<div class="hoax-row">' +
                    '<a target="_blank" href="http://www.politifact.com' + data[i].doc.body_link + '">' +
                    '<div class="row-content">' +
                    '<div class="' + classMeter + ' meter-type meter-bar"><p>'+ data[i].doc.meter_detail +'</p></div>' +
                    '<div class="block-img">' +
                    '<img src="' + data[i].doc.body_authorImg + '" width="100" alt="' + data[i].doc.body_detail + '">' +
                    '</div>' +
                    '<div class="block-text">' +
                    '<h4>' + data[i].doc.body_detail + '</h4>' +
                    '<p>' + jQuery.trim(data[i].doc.title).substring(0, 50)
                        .split(" ").slice(0, -1).join(" ") + "..." + '</p>' +
                    '<span>' + data[i].doc.body_date + '</span></div></div><p class="score">'+ data[i].score + '</p></a></div>');

        }
    } else {
        $('#content .hoax-rows').append('<h3>Sorry! No result for this query</h3>');
    }

    resultatTest(data.length);
}

/**
 * Pagination
 */
function updatePagination() {
    $("div.holder").jPages({
        containerID: "rows-item",
        perPage: 9,
        animation: 'fadeInLeft',
        previous: "⟵",
        next: "⟶",
        minHeight: false,
        delay: 0
    });
}

function resultatTest(nb) {
    var nbResult = $('#content .hoax-rows .hoax-row').length;
    $('.wrap-sub-search .text-result').text(nb +' Results');
}

$('.filter-icon').click(function () {
    $('.filter-search').toggleClass('open');
});
